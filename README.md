# cartographie

Ce projet a pour fonction de regrouper l'ensemble des données importées dans les cartes Umap utilisées par l'association.

# Utilisation de gitlab
## Installer Git
Git est un logiciel permettant de faire une copie des fichiers stockés sur gitlab sur votre ordinateur et les synchroniser au fur et à mesure de vos modifications ou celles des autres.

Sur windows, téléchargez le logiciel sur le site [git](https://git-scm.com/download/windows)
Sur linux, installer git via le gestionnaire de logiciel ou votre terminal avec les commandes suivantes :
* Ubuntu :
`apt-get install git`
* Fedora :
`dnf install git`
* Arch Linux / Manajro :
`pacman -S git`

Lors de l'installation ou de la première execution, certaines informations peuvent vous être demander. Si un choix d'éditeurs de texte vous est proposé, préférez notepad++ sur windows (et [l'installer](https://notepad-plus-plus.org/downloads) si ce n'est pas fait), sur linux choisir un éditeur de texte qui n'est pas Vim, un peu complexe pour un utilisateur non averti.

## Cloner le dépot
Ouvrir Git Bash sur windows ou le terminal sur linux puis rendez vous dans le dossier où vous voulez ajouter la copie du dépot. La commande à utiliser est `cd` suivi du nom du dossier dans lequel vous voulez vous rendre, `cd ~` vous amène dans votre dossier personnel.
La commande `ls` permet d'afficher les fichiers et dossiers présents dans votre répertoire courant.

Ensuite cloner le dépot :
`git clone https://gitlab.com/veli-velo/cartographie.git`

Un dossier cartographie doit apparaître dans votre dossier courant avec l'ensemble des dossiers et fichiers présents sur le dépot gitlab.

## Obtenir les dernières modifications
A partir de git bash ou de votre terminal, allez dans le répertoire cartographie (commande `cd ...`) puis tapez la commande `git pull`

## Ajouter / modifier des fichiers
Ajoutez ou modifiez des fichiers dans le répertoire cartographie ou un de ses sous répertoire.
Ensuite, dans git bash (windows) ou votre terminal (linux), vous rendre dans le répertoire cartographie (commande `cd`).
Pour afficher la liste des modifications, taper la commande `git status`. Deux catégories de modifications apparaissent :
* En vert les fichiers prêts à être enregistrer.
* En rouge les fichiers modifiés depuis la dernière sauvegarde ou les nouveaux fichiers (ou dossiers)
Pour sauvegarder votre travail, il faut que tous les fichiers que vous voulez enregistrer soient en vert, pour celà utiliser la commande `git add CHEMIN_ET_NOM_DU_FICHIER`. La valeur CHEMIN_ET_NOM_DU_FICHIER est le chemin relatif du fichier depuis le répertoire cartographie, c'est celui affiché lors du `git status`.

Une fois tous vos fichiers et dossiers en vert sur une commande `git status`, vous pouvez utiliser la commande `git commit`, un éditeur de texte dois s'ouvrir pour renseigner un résumer des modifications. Sauvegardez puis fermer l'éditeur, vos modifications sont maintenant sauvegardées en local.
Pour envoyer les données au serveur gitlab, faites d'abord un `git pull` pour récupérer les dernières modifications puis un `git push`

# Avancement des calques
- [ ] Propositions pistes cyclables
- [ ] Propositions voies vertes
- [X] Propositions DSC
- [X] DSC existants
- [x] Propositions tourne à droite
- [x] Tourne à droite existants
- [ ] Propositions couloirs de bus partagés avec les vélos
- [ ] Propositions bandes cyclables
- [ ] Propositions sas à vélo
- [X] Sas à vélo existants
- [X] Propositions stationnements
- [X] stationnements existants
