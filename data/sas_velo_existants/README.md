# Obtenir les données OpenStreetMap
Se rendre sur le site [Overpass Turbo](https://overpass-turbo.eu), copier-coller le texte présent dans le fichier requete_overpass.txt dans la zone de texte à gauche.

Appuyer sur le bouton 'Exécuter'. Après quelques secondes les résultats apparaissent sur la carte dans la zone de droite.
Cliquer sur le bouton 'Exporter', puis sur 'Télécharger' de la ligne 'Télécharger/copier en tant que GeoJSON' (enregistrer avec le nom 'export_openstreetmap.geojson').
# Importer les données dans un calque Umap
Sur la carte, bouton 'importer des données', sélectionnez le fichier, sélectionnez le calque à utiliser, cochez la case 'Remplacer le contenu du calque' puis le bouton 'Importer'
# Ajouter des stationnements sur Openstreetmap
[Wiki OpenStreetMap](https://wiki.openstreetmap.org/wiki/FR:Bicycle#Sas_cyclable)

**Partie à compléter**
