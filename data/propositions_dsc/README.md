# Construction du fichier
Les propositions des membres sont disponibles sur le [framacalc](https://lite.framacalc.org/velivelo_arceaux_velos), colonnes "G", "H" et "I".
Les données à afficher étant linéaires, il est nécessaire de créer un fichier geojson.

# Création du fichier geojson
Se rendre sur le site [Overpass Turbo](https://overpass-turbo.eu) et copier-coller le texte présent dans le fichier requete_overpass.txt dans la zone de texte à gauche.
Remplacer `Rue du Clos Augier` par le nom de la rue recherchée.
Appuyer sur le bouton 'Exécuter'. Après quelques secondes les résultats apparaissent sur la carte dans la zone de droite.
Cliquer sur le bouton 'Exporter', puis sur 'Télécharger' de la ligne 'Télécharger/copier en tant que GeoJSON' (enregistrer avec le nom 'Proposition_dsc.geojson').

Le fichier devrait ressembler à :
``` json
{
  "type": "FeatureCollection",
  "generator": "overpass-ide",
  "copyright": "The data included in this document is from www.openstreetmap.org. The data is made available under ODbL.",
  "timestamp": "2019-11-20T20:01:02Z",
  "features": [
    {
      "type": "Feature",
      "properties": {
        "@id": "way/31301503",
        "highway": "residential",
        "name": "Rue du Clos Augier",
        "oneway": "yes",
        "source": "cadastre-dgi-fr source : Direction Générale des Impôts - Cadastre ; mise à jour : 2009",
        "surface": "asphalt"
      },
      "geometry": {
        "type": "LineString",
        "coordinates": [
          [
            1.236777,
            45.828802
          ],
          [
            1.2366688,
            45.8286981
          ],
          [
            1.2366383,
            45.8286688
          ],
          [
            1.2356678,
            45.827737
          ]
        ]
      },
      "id": "way/31301503"
    },
    {
      "type": "Feature",
      "properties": {
        "@id": "way/745983258",
        "highway": "residential",
        "name": "Rue du Clos Augier",
        "oneway": "yes",
        "surface": "asphalt"
      },
      "geometry": {
        "type": "LineString",
        "coordinates": [
          [
            1.2372938,
            45.8293005
          ],
          [
            1.236777,
            45.828802
          ]
        ]
      },
      "id": "way/745983258"
    }
  ]
}
```

Compléter en ajoutant un champ `"commentaire": "Commentaire présent en ligne I"` entre les crochets `"properties": {...}`, ne pas oublier d'ajouter une virgule à la fin de la ligne précédente.
Le fichier doit maintenant ressembler à (dans l'exemple la rue est divisée, il est nécessaire d'ajouter le champ commentaire deux fois) :
```json
{
  "type": "FeatureCollection",
  "generator": "overpass-ide",
  "copyright": "The data included in this document is from www.openstreetmap.org. The data is made available under ODbL.",
  "timestamp": "2019-11-20T20:01:02Z",
  "features": [
    {
      "type": "Feature",
      "properties": {
        "@id": "way/31301503",
        "highway": "residential",
        "name": "Rue du Clos Augier",
        "oneway": "yes",
        "source": "cadastre-dgi-fr source : Direction Générale des Impôts - Cadastre ; mise à jour : 2009",
        "surface": "asphalt",
        "commentaire": "La rue a été refaite est mise en sein unique en 2018. Le double sens permettrait de rejoindre la Rue d'Antony puis le quartier Carnot sans remonter toute l'avenue Armand Dutreix"
      },
      "geometry": {
        "type": "LineString",
        "coordinates": [
          [
            1.236777,
            45.828802
          ],
          [
            1.2366688,
            45.8286981
          ],
          [
            1.2366383,
            45.8286688
          ],
          [
            1.2356678,
            45.827737
          ]
        ]
      },
      "id": "way/31301503"
    },
    {
      "type": "Feature",
      "properties": {
        "@id": "way/745983258",
        "highway": "residential",
        "name": "Rue du Clos Augier",
        "oneway": "yes",
        "surface": "asphalt",
        "commentaire": "La rue a été refaite est mise en sein unique en 2018. Le double sens permettrait de rejoindre la Rue d'Antony puis le quartier Carnot sans remonter toute l'avenue Armand Dutreix"
      },
      "geometry": {
        "type": "LineString",
        "coordinates": [
          [
            1.2372938,
            45.8293005
          ],
          [
            1.236777,
            45.828802
          ]
        ]
      },
      "id": "way/745983258"
    }
  ]
}
```

# Ajouter des propositions
Même procédure que pour la création du fichier, sauf qu'après avoir exécuter la requête, cliquer sur 'Exporter', 'Données', Copier en tant que requête GEOJSON.
Ouvrir un éditeur de texte et coller le presse papier.
Supprimer ensuite les 6 premières lignes :
```json
{
  "type": "FeatureCollection",
  "generator": "overpass-ide",
  "copyright": "The data included in this document is from www.openstreetmap.org. The data is made available under ODbL.",
  "timestamp": "2019-11-20T20:01:02Z",
  "features": [
```
Ainsi que les 2 dernières lignes :
```json
  ]
}
```
Ajouter le champ commentaire avec les informations complémentaires du framacalc (colonne I)
Copier le texte restant, puis dans le fichier Proposition_dsc.geojson ajouter une virgule à la fin de la 3e ligne à partir de la la fin du fichier, puis coller le presse papier.

Le fichier modifié est alors le suivant :
```json
{
  "type": "FeatureCollection",
  "generator": "overpass-ide",
  "copyright": "The data included in this document is from www.openstreetmap.org. The data is made available under ODbL.",
  "timestamp": "2019-11-20T20:01:02Z",
  "features": [
    {
      "type": "Feature",
      "properties": {
        "@id": "way/31301503",
        "highway": "residential",
        "name": "Rue du Clos Augier",
        "oneway": "yes",
        "source": "cadastre-dgi-fr source : Direction Générale des Impôts - Cadastre ; mise à jour : 2009",
        "surface": "asphalt",
        "commentaire": "La rue a été refaite est mise en sein unique en 2018. Le double sens permettrait de rejoindre la Rue d'Antony puis le quartier Carnot sans remonter toute l'avenue Armand Dutreix"
      },
      "geometry": {
        "type": "LineString",
        "coordinates": [
          [
            1.236777,
            45.828802
          ],
          [
            1.2366688,
            45.8286981
          ],
          [
            1.2366383,
            45.8286688
          ],
          [
            1.2356678,
            45.827737
          ]
        ]
      },
      "id": "way/31301503"
    },
    {
      "type": "Feature",
      "properties": {
        "@id": "way/745983258",
        "highway": "residential",
        "name": "Rue du Clos Augier",
        "oneway": "yes",
        "surface": "asphalt",
        "commentaire": "La rue a été refaite est mise en sein unique en 2018. Le double sens permettrait de rejoindre la Rue d'Antony puis le quartier Carnot sans remonter toute l'avenue Armand Dutreix"
      },
      "geometry": {
        "type": "LineString",
        "coordinates": [
          [
            1.2372938,
            45.8293005
          ],
          [
            1.236777,
            45.828802
          ]
        ]
      },
      "id": "way/745983258"
    },
    {
      "type": "Feature",
      "properties": {
        "@id": "way/31304681",
        "highway": "residential",
        "lit": "yes",
        "name": "Rue Rabelais",
        "oneway": "yes",
        "source": "cadastre-dgi-fr source : Direction Générale des Impôts - Cadastre ; mise à jour : 2009",
        "surface": "asphalt",
		    "commentaire": "Permet d'éviter des itinéraires plus long et plus dénivelés en sortant du Lycée Dautry. Largeur suffisante."
      },
      "geometry": {
        "type": "LineString",
        "coordinates": [
          [
            1.2748519,
            45.8402137
          ],
          [
            1.2748901,
            45.8401813
          ],
          [
            1.2768953,
            45.8384104
          ],
          [
            1.276937,
            45.8383644
          ]
        ]
      },
      "id": "way/31304681"
    },
    {
      "type": "Feature",
      "properties": {
        "@id": "way/481200194",
        "highway": "residential",
        "lit": "yes",
        "maxspeed:type": "FR:urban",
        "name": "Rue Rabelais",
        "oneway": "yes",
        "source": "cadastre-dgi-fr source : Direction Générale des Impôts - Cadastre ; mise à jour : 2009",
        "surface": "asphalt",
		    "commentaire": "Permet d'éviter des itinéraires plus long et plus dénivelés en sortant du Lycée Dautry. Largeur suffisante."
      },
      "geometry": {
        "type": "LineString",
        "coordinates": [
          [
            1.276937,
            45.8383644
          ],
          [
            1.2770227,
            45.8382697
          ],
          [
            1.2772056,
            45.8382405
          ]
        ]
      },
      "id": "way/481200194"
    }
  ]
}
```

# Import du fichier dans un calque Umap
Umap n'accepte pas les lignes sans coordonnées géographiques, il faut les supprimer.
Ensuite sur la carte, bouton 'importer des données', sélectionnez le fichier, sélectionnez le calque à utiliser, cochez la case 'Remplacer le contenu du calque' puis le bouton 'Importer'
