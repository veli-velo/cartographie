# Obtenir les données OpenStreetMap
Se rendre sur le site [Overpass Turbo](https://overpass-turbo.eu), copier-coller le texte présent dans le fichier requete_overpass.txt dans la zone de texte à gauche.

Appuyer sur le bouton 'Exécuter'. Après quelques secondes les résultats apparaissent sur la carte dans la zone de droite.
Cliquer sur le bouton 'Exporter', puis sur 'Télécharger' de la ligne 'Télécharger/copier en tant que GeoJSON' (enregistrer avec le nom 'export_openstreetmap.geojson').
# Apporter des modifications aux données
Les attributs OpenStreetMap ont des valeurs en anglais et pas forcement compréhensible, un script permet d'ajouter des informations complémentaires à partir des attributs existants.
Pour cela lancer le fichier `run_scripts.bat` (vérifier les [pré-requis](../../scripts/stationnements_osm/README.md) avant).
Un fichier data_umap.geojson devrait être créé (ou modifié si déjà existant).
# Importer les données dans un calque Umap
Sur la carte, bouton 'importer des données', sélectionnez le fichier, sélectionnez le calque à utiliser, cochez la case 'Remplacer le contenu du calque' puis le bouton 'Importer'
# Ajouter des stationnements sur Openstreetmap
Se rendre sur ce [thème mapcontrib](https://www.mapcontrib.xyz/t/659388-Stationnements_cyclables_Limoges_Metropole). Se connecter avec un compte Openstreetmap.
Pour ajouter un stationnement, cliquer sur le bouton 'Ajouter une donnée manquante' (icone en forme de crayon, menu de droite).

Déplacer la carte de manière à avoir la croix au centre de la carte à l'emplacement du stationnement à ajouter, cliquer sur 'Suivant'.
Choisir le type de stationnement afin d'avoir les attributs pré-configurés ou 'Ajout de tags libre' pour entrer tous les tags manuellement.. Se connecter avec un compte Openstreetmap.

Les attributs et leur valeurs à utiliser sont disponible sur le [wiki openstreetmap](https://wiki.openstreetmap.org/wiki/FR:Tag:amenity%3Dbicycle_parking).
