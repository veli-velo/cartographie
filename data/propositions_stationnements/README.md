# Construction du fichier
## Récupération des propositions
Les propositions des membres sont disponibles sur le [framacalc](https://lite.framacalc.org/velivelo_arceaux_velos).
Les colonnes 'Commune', 'Adresse', 'Précisions' et 'nombre' sont copiées et collées dans un tableur (ex : Libreoffice Calc). La colonne 'Précisions' est renommée en 'Precisions' (pas d'accent).
Ajout de deux colonnes 'lat' et 'lon' pour ajouter les coordonnées géographiques.
Les propositions comportant plusieurs lieux (ex : les parcs de Limoges) sont décomposées sur plusieurs lignes.
## Ajout des coordonnées
Pour trouver la latitude et la longitude d'un point, se rendre sur le site [openstreetmap.org](www.openstreetmap.org), faire un clic droit sur le point à positionner puis clic sur 'Afficher l'adresse'. Les coordonnées s'affichent alors dans le champ de recherche en haut à gauche.

# Import du fichier dans un calque Umap
Umap n'accepte pas les lignes sans coordonnées géographiques, il faut les supprimer.
Ensuite sur la carte, bouton 'importer des données', sélectionnez le fichier, sélectionnez le calque à utiliser, cochez la case 'Remplacer le contenu du calque' puis le bouton 'Importer'
