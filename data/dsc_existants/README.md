# Obtenir les données Openstreetmap
Se rendre sur le site [Overpass Turbo](https://overpass-turbo.eu), copier-coller le texte présent dans le fichier requete_overpass.txt dans la zone de texte à gauche.

Appuyer sur le bouton 'Exécuter'. Après quelques secondes les résultats apparaissent sur la carte dans la zone de droite.
Cliquer sur le bouton 'Exporter', puis sur 'Télécharger' de la ligne 'Télécharger/copier en tant que GeoJSON' (enregistrer avec le nom 'export_openstreetmap.geojson').

# Explication de la requête
La [documentation OpenStreetMap](https://wiki.openstreetmap.org/wiki/FR:Bicycle#Bandes_cyclables_sur_les_routes_.C3.A0_sens_unique) consacrée à la circulation des vélos sur les voies à sens uniques liste plusieurs possibilités, les aménagements rencontrés à Limoges correspondent aux cas suivants :
* M3a - Double sens cyclable avec bande cyclable dans le sens opposé à la circulation :
1. `highway=*` + `oneway=yes` + `cycleway:left=lane` + `cycleway:left:oneway=-1`
2. `highway=*` + `oneway=yes` + `cycleway=opposite_lane`
* S1 - Cyclistes autorisés à circuler en contre sens
1. `highway=*` + `oneway=yes` + `oneway:bicycle=no`
2. `highway=*` + `oneway=yes` + `cycleway=opposite`
Le choix est fait de ne garder que les valeurs de highway suivants : primary, secondary, tertiary, unclassified, residential et living_street.
La requête correspondante en utilisant l'assistant Overpass Turbo : (highway=primary or highway=secondary or highway=tertiary or highway=unclassified or highway=residential or highway=living_street) and oneway=yes and (oneway:bicycle=no or cycleway=opposite or cycleway=opposite_lane or (cycleway:left=lane and cycleway:left:oneway=-1) or (cycleway:right=lane and cycleway:lane:oneway=-1)) in "Communauté Urbaine Limoges Métropole"

# Importer les données dans un calque Umap
Sur la carte, bouton 'importer des données', sélectionnez le fichier, sélectionnez le calque à utiliser, cochez la case 'Remplacer le contenu du calque' puis le bouton 'Importer'.
