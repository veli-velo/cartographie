from unittest import TestCase
import json_modification


class TestAdd_attribut_to_json(TestCase):
    def test_add_attribut_to_json_should_return_json_in_parameter_with_Add1_addition_when_the_json_conditions_are_strictly_the_json_values(self):
        serialize_json = '{"attribut1": "valeur1"}'
        modification_json = '{"actions":[{"type":"add","conditions":{"attribut1":"valeur1"},"modifications":{"Add1":"Value adding 1"}}]}'
        modified_json, error_code = json_modification.set_modification_to_json(serialize_json, modification_json)
        self.assertEqual(error_code, 0)
        self.assertEqual(modified_json, '{"attribut1": "valeur1", "Add1": "Value adding 1"}')

    def test_add_attribut_to_json_should_return_same_json_than_one_in_parameter_when_the_attribut_conditions_are_differents_than_the_json_attibut(self):
        serialize_json = '{"attribut1": "valeur1"}'
        modification_json = '{"actions":[{"type":"add","conditions":{"attribut2":"valeur1"},"modifications":{"Add1":"Value adding 1"}}]}'
        modified_json, error_code = json_modification.set_modification_to_json(serialize_json, modification_json)
        self.assertEqual(error_code, 0)
        self.assertEqual(modified_json, '{"attribut1": "valeur1"}')

    def test_add_attribut_to_json_should_return_same_json_than_one_in_parameter_when_the_value_conditions_are_differents_than_the_json_value(self):
        serialize_json = '{"attribut1": "valeur1"}'
        modification_json = '{"actions":[{"type":"add","conditions":{"attribut1":"valeur2"},"modifications":{"Add1":"Value adding 1"}}]}'
        modified_json, error_code = json_modification.set_modification_to_json(serialize_json, modification_json)
        self.assertEqual(error_code, 0)
        self.assertEqual(modified_json, '{"attribut1": "valeur1"}')

    def test_add_attribut_to_json_should_return_same_json_than_one_in_parameter_when_the_attribut_and_value_conditions_are_differents_than_the_json(self):
        serialize_json = '{"attribut1": "valeur1"}'
        modification_json = '{"actions":[{"type":"add","conditions":{"attribut2":"valeur2"},"modifications":{"Add1":"Value adding 1"}}]}'
        modified_json, error_code = json_modification.set_modification_to_json(serialize_json, modification_json)
        self.assertEqual(error_code, 0)
        self.assertEqual(modified_json, '{"attribut1": "valeur1"}')

    def test_add_attribut_to_json_should_return_json_in_paramater_with_Add1_addition_when_the_json_have_several_attribut_and_one_of_them_are_equal_to_json_conditions(self):
        serialize_json = '{"attribut1": "valeur1", "attribut2": "valeur2"}'
        modification_json = '{"actions":[{"type":"add","conditions":{"attribut1":"valeur1"},"modifications":{"Add1":"Value adding 1"}}]}'
        modified_json, error_code = json_modification.set_modification_to_json(serialize_json, modification_json)
        self.assertEqual(error_code, 0)
        self.assertEqual(modified_json, '{"attribut1": "valeur1", "attribut2": "valeur2", "Add1": "Value adding 1"}')

    def test_add_attribut_to_json_should_return_same_json_in_paramater_when_the_json_have_several_attribut_and_conditions_are_not_compliant(self):
        serialize_json = '{"attribut1": "valeur1", "attribut2": "valeur2"}'
        modification_json = '{"actions":[{"type":"add","conditions":{"attribut1":"valeur2"},"modifications":{"Add1":"Value adding 1"}}]}'
        modified_json, error_code = json_modification.set_modification_to_json(serialize_json, modification_json)
        self.assertEqual(error_code, 0)
        self.assertEqual(modified_json, '{"attribut1": "valeur1", "attribut2": "valeur2"}')

    def test_add_attribut_to_json_should_return_json_in_paramater_with_Add1_addition_when_the_json_and_conditions_have_several_attributs_and_conditions_are_compliant(self):
        serialize_json = '{"attribut1": "valeur1", "attribut2": "valeur2", "attribut3": "valeur3"}'
        modification_json = '{"actions":[{"type":"add","conditions":{"attribut1":"valeur1", "attribut3":"valeur3"},"modifications":{"Add1":"Value adding 1"}}]}'
        modified_json, error_code = json_modification.set_modification_to_json(serialize_json, modification_json)
        self.assertEqual(error_code, 0)
        self.assertEqual(modified_json, '{"attribut1": "valeur1", "attribut2": "valeur2", "attribut3": "valeur3", "Add1": "Value adding 1"}')

    def test_add_attribut_to_json_should_return_same_json_when_the_json_and_conditions_have_several_attributs_and_conditions_are_not_compliant(self):
        serialize_json = '{"attribut1": "valeur1", "attribut2": "valeur2", "attribut3": "valeur3"}'
        modification_json = '{"actions":[{"type":"add","conditions":{"attribut1":"valeur1", "attribut3":"valeur2"},"modifications":{"Add1":"Value adding 1"}}]}'
        modified_json, error_code = json_modification.set_modification_to_json(serialize_json, modification_json)
        self.assertEqual(error_code, 0)
        self.assertEqual(modified_json, '{"attribut1": "valeur1", "attribut2": "valeur2", "attribut3": "valeur3"}')

    def test_add_attribut_to_json_should_return_json_in_paramater_with_all_additions_when_the_json_and_conditions_have_several_attributs_and_conditions_are_compliant(self):
        serialize_json = '{"attribut1": "valeur1", "attribut2": "valeur2", "attribut3": "valeur3"}'
        modification_json = '{"actions":[{"type":"add","conditions":{"attribut1":"valeur1", "attribut3":"valeur3"},"modifications":{"Add1":"Value adding 1", "Add2":"Value adding 2"}}]}'
        modified_json, error_code = json_modification.set_modification_to_json(serialize_json, modification_json)
        self.assertEqual(error_code, 0)
        self.assertEqual(modified_json, '{"attribut1": "valeur1", "attribut2": "valeur2", "attribut3": "valeur3", "Add1": "Value adding 1", "Add2": "Value adding 2"}')

    def test_add_attribut_to_json_should_return_json_in_paramater_with_one_addition_when_there_are_severals_actions_and_only_one_with_conditions_true(self):
        serialize_json = '{"attribut1": "valeur1", "attribut2": "valeur2", "attribut3": "valeur3"}'
        modification_json = '{"actions":[{"type":"add","conditions":{"attribut1":"valeur2", "attribut3":"valeur3"},"modifications":{"Add1":"Value adding 1", "Add2":"Value adding 2"}}, {"type":"add","conditions":{"attribut1":"valeur1", "attribut3":"valeur3"},"modifications":{"Add3":"Value adding 3", "Add4":"Value adding 4"}}]}'
        modified_json, error_code = json_modification.set_modification_to_json(serialize_json, modification_json)
        self.assertEqual(error_code, 0)
        self.assertEqual(modified_json, '{"attribut1": "valeur1", "attribut2": "valeur2", "attribut3": "valeur3", "Add3": "Value adding 3", "Add4": "Value adding 4"}')

    def test_add_attribut_to_json_should_return_json_in_paramater_with_one_addition_when_conditional_part_is_into_an_json_object(self):
        serialize_json = '{"attribut1": "valeur1", "attribut2": {"attribut3": "valeur3"}}'
        modification_json = '{"actions":[{"type":"add","conditions":{"attribut3":"valeur3"},"modifications":{"Add1":"Value adding 1"}}]}'
        modified_json, error_code = json_modification.set_modification_to_json(serialize_json, modification_json)
        self.assertEqual(error_code, 0)
        self.assertEqual(modified_json, '{"attribut1": "valeur1", "attribut2": {"attribut3": "valeur3", "Add1": "Value adding 1"}}')
