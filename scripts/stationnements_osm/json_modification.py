#!/usr/bin/python
# coding: utf8

import json


def set_modification_to_json(json_string, json_modifications, pretty_format=False):
    #TODO : vérification de la validité des json
    json_object = json.loads(json_string)
    modifications_object = json.loads(json_modifications)

    for action in modifications_object['actions']:
        look_for_conditions_true_and_add_modifications(json_object, action['conditions'],
                                                       action['modifications'])

    if not pretty_format:
        indentation = None
    else:
        indentation = 2
    new_json_string = json.dumps(json_object, ensure_ascii=False, indent=indentation)
    return new_json_string, 0


def look_for_conditions_true_and_add_modifications(json_object_to_modified, json_conditions, json_modifications):
    if type(json_object_to_modified) is list:
        for item in json_object_to_modified:
            look_for_conditions_true_and_add_modifications(item, json_conditions, json_modifications)
        return 0
    elif type(json_object_to_modified) is dict:
        counter_of_true_conditions = 0
        for attribut in json_conditions.keys():
            if conditional_attribut_is_in_tested_json(json_object_to_modified, attribut):
                if type(json_object_to_modified[attribut]) is str:
                    if conditional_value_is_in_tested_json(json_object_to_modified, json_conditions, attribut):
                        counter_of_true_conditions = counter_of_true_conditions + 1

        if counter_of_true_conditions == len(json_conditions):
            add_all_modifications_in_json(json_object_to_modified, json_modifications)

        for attribut in json_object_to_modified.keys():
            if analyse_item_is_an_object(json_object_to_modified[attribut]):
                look_for_conditions_true_and_add_modifications(json_object_to_modified[attribut], json_conditions, json_modifications)
        return 0


def analyse_item_is_an_object(item):
    return type(item) is dict or type(item) is list


def conditional_attribut_is_in_tested_json(json_tested, attribut_to_test):
    return attribut_to_test in json_tested


def conditional_value_is_in_tested_json(json_tested, value_to_test, attribut_name):
    return value_to_test[attribut_name] == json_tested[attribut_name]


def add_all_modifications_in_json(json_to_modifie, modifications):
    for modif in modifications.keys():
        json_to_modifie[modif] = modifications[modif]
