# Pré-requis
Ce script nécessite d'avoir [Python](https://www.python.org/downloads/), au minimum la version 3.
Lors de l'installation vérifier que la case de l'ajout aux variables d'environnement est cochée. Sinon ajouter
manuellement les chemins aux variables d'environnement.
# Le fichier de configuration
Le fichier de configuration est un fichier au format json contenant un tableau d'éléments `actions`, il ressemble à ceci avec un seul élément :
```json
{
  "actions": [
    {
      "type": "add",
      "conditions": {
        "amenity": "bicycle_parking",
        "bicycle_parking": "lockers"
      },
      "modifications": {
        "bicycle_parking_fr": "Casiers individuels verrouillés",
        "picture_url": "https://framapic.org/vVU93zU5QZse/qIZhXFw3WSFF.JPG"
      }
    }
  ]
}
```
Un objet actions est composé de :
* un `type`, aujourd'hui seul la valeur 'add' est supportée (ajout d'informations dans le fichier)
* une ou des `conditions`, il s'agit des attributs OpenStreetMap étant concernés par la modification (un 'et' logique s'applique entre ces différents éléments)
* une ou des `modifications`, les attributs ajoutés dans le fichier modifié