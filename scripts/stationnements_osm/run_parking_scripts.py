#!/usr/bin/python
# coding: utf8

import json_modification
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Ajoute des informations dans le json d'entrée")
    parser.add_argument('-i', '--input', help='L\'emplacement du fichier geojson à modifier', required=True)
    parser.add_argument('-m', '--modifications',
                        help='L\'emplacement du fichier json contenant les modifications à effectuer', required=True)
    parser.add_argument('-o', '--output', help='Le nom du fichier de sortie du script')

    args = parser.parse_args()

    input_is_open = True
    modifications_is_open = True
    try:
        json_to_modify = open(args.input, 'r', encoding='utf8')
    except OSError:
        print('Le fichier {} n\'existe pas ou contient une erreur dans son chemin'.format(args.input))
        input_is_open = False

    try:
        list_of_modifications = open(args.modifications, 'r', encoding='utf8')
    except OSError:
        print('Le fichier {} n\'existe pas ou contient une erreur dans son chemin'.format(args.modifications))
        modifications_is_open = False

    if modifications_is_open and input_is_open is False:
        list_of_modifications.close()

    if input_is_open and modifications_is_open is False:
        json_to_modify.close()

    if modifications_is_open is False or input_is_open is False:
        exit(1)

    string_to_write, modification_result = json_modification.set_modification_to_json(json_to_modify.read(),
                                                                                      list_of_modifications.read(),
                                                                                      True)
    json_to_modify.close()
    list_of_modifications.close()
    #print(string_to_write)
    if args.output is None:
        out = open('output.geojson', 'w', encoding='utf8')
    else:
        out = open(args.output, 'w', encoding='utf8')
    out.write(string_to_write)
    out.close()
