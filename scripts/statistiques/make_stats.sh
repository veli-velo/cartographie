#!/bin/sh

jq -r -f limoges_metropole_capacity.jq < list_bicycle_parking.geojson >> stats_stationnements.txt
texte=" La répartition est la suivante :"
echo $texte >> stats_stationnements.txt
jq -r -f capacity_per_city.jq < list_bicycle_parking.geojson >> stats_stationnements.txt
